#pragma once

namespace llmo::assembler {
	namespace branch::detail {
		struct BLX {
			std::uint32_t addr : 24;
			std::uint32_t H	   : 1;
			std::uint32_t L	   : 1;
			std::uint32_t unk  : 1;
			std::uint32_t X	   : 1;
			std::uint32_t cond : 4;
		};
		constexpr std::uint32_t makeBranch( std::uint32_t from, std::uint32_t to, bool L = false, bool X = false ) {
			std::uint32_t flag = 0xB800;
			if ( X ) L = true;
			if ( L ) {
				if ( X )
					flag = 0xE800;
				else
					flag = 0xF800;
			}
			return ( ( ( to - from - 4 ) >> 12 ) & 0x7FF ) | 0xF000 | ( ( ( ( ( to - from - 4 ) >> 1 ) & 0x7FF ) | flag ) << 16 );
		}
		static_assert( makeBranch( 0, 0x186a0 ) == 0xbb4ef018 );
		static_assert( makeBranch( 0, 0x186a0, true ) == 0xfb4ef018 );
		static_assert( makeBranch( 0, 0x186a0, true, true ) == 0xeb4ef018 );
		static_assert( makeBranch( 0, 0x186a0, false, true ) == 0xeb4ef018 );
	} // namespace branch::detail

	namespace detail {
		int MOV( uintptr_t addr, int word, int reg, bool w_or_t ) {
			*(uint8_t *)( addr + 2 ) = ( (uint8_t *)&word )[0]; // lobyte
			auto hibyte = ( (uint8_t *)&word )[1];
			*(uint8_t *)( addr ) = ( w_or_t ? 0xC0 : 0x40 ) + ( hibyte / 0x10 );
			if ( hibyte - ( ( hibyte / 0x10 ) * 0x10 ) < 0x8 ) {
				*(uint8_t *)( addr + 1 ) = 0xF2;
				*(uint8_t *)( addr + 3 ) = ( hibyte - ( ( hibyte / 0x10 ) * 0x10 ) ) * 0x10 + reg;
			} else {
				*(uint8_t *)( addr + 1 ) = 0xF6;
				*(uint8_t *)( addr + 3 ) = ( ( hibyte - ( ( hibyte / 0x10 ) * 0x10 ) ) - 0x8 ) * 0x10 + reg;
			}
			return 4;
		}
		int MOV( uintptr_t addr, int dword, platforms::cpu::registers reg ) {
			MOV( addr, ( (uint16_t *)&dword )[0], (int)reg, false ); // MOVW
			MOV( addr + 4, ( (uint16_t *)&dword )[1], (int)reg, true ); // MOVT
			return 8;
		}

		int writeRegister( void *from, void *to, platforms::cpu::registers reg ) {
			if ( reg == platforms::cpu::registers::r0 ) {
				*(std::uint8_t *)from = 0x02;
				*( (std::uint8_t *)from + 1 ) = 0xb4;
				MOV( (std::uintptr_t)from + 2, (int)to, platforms::cpu::registers::r1 );
				*( (std::uint8_t *)from + 10 ) = 0x08;
				*( (std::uint8_t *)from + 11 ) = 0x60;
				*( (std::uint8_t *)from + 12 ) = 0x02;
				*( (std::uint8_t *)from + 13 ) = 0xbc;
			} else {
				*(std::uint8_t *)from = 0x01;
				*( (std::uint8_t *)from + 1 ) = 0xb4;
				MOV( (std::uintptr_t)from + 2, (int)to, platforms::cpu::registers::r0 );
				if ( reg < platforms::cpu::registers::r8 ) {
					*( (std::uint8_t *)from + 10 ) = (int)reg;
					*( (std::uint8_t *)from + 11 ) = 0x60;
					*( (std::uint8_t *)from + 12 ) = 0x01;
					*( (std::uint8_t *)from + 13 ) = 0xbc;
					return 14;
				} else {
					*( (std::uint8_t *)from + 10 ) = 0xc0;
					*( (std::uint8_t *)from + 11 ) = 0xf8;
					*( (std::uint8_t *)from + 12 ) = 0x00;
					*( (std::uint8_t *)from + 13 ) = (int)reg * 0x10;
					*( (std::uint8_t *)from + 14 ) = 0x01;
					*( (std::uint8_t *)from + 15 ) = 0xbc;
					return 16;
				}
			}
			return 0;
		}
		int readRegister( void *from, void *to, platforms::cpu::registers reg ) {
			if ( reg == platforms::cpu::registers::r0 ) {
				*(std::uint8_t *)from = 0x02;
				*( (std::uint8_t *)from + 1 ) = 0xb4;
				MOV( (std::uintptr_t)from + 2, (int)to, platforms::cpu::registers::r1 );
				*( (std::uint8_t *)from + 10 ) = 0x08;
				*( (std::uint8_t *)from + 11 ) = 0x68;
				*( (std::uint8_t *)from + 12 ) = 0x02;
				*( (std::uint8_t *)from + 13 ) = 0xbc;
			} else {
				*(std::uint8_t *)from = 0x01;
				*( (std::uint8_t *)from + 1 ) = 0xb4;
				MOV( (std::uintptr_t)from + 2, (int)to, platforms::cpu::registers::r0 );
				if ( reg < platforms::cpu::registers::r8 ) {
					*( (std::uint8_t *)from + 10 ) = (int)reg;
					*( (std::uint8_t *)from + 11 ) = 0x68;
					*( (std::uint8_t *)from + 12 ) = 0x01;
					*( (std::uint8_t *)from + 13 ) = 0xbc;
					return 14;
				} else {
					*( (std::uint8_t *)from + 10 ) = 0xd0;
					*( (std::uint8_t *)from + 11 ) = 0xf8;
					*( (std::uint8_t *)from + 12 ) = 0x00;
					*( (std::uint8_t *)from + 13 ) = (int)reg * 0x10;
					*( (std::uint8_t *)from + 14 ) = 0x01;
					*( (std::uint8_t *)from + 15 ) = 0xbc;
					return 16;
				}
			}
			return 0;
		}
		int writeScratchRegisters( void *from, void *to ) {
			int i = 0;
			int offset = 0;
			for ( auto reg : platforms::calling_convention::scratch_registers )
				offset += writeRegister( (std::uint8_t *)from + offset, (std::uint8_t *)to + sizeof( void * ) * i++, reg );
			return offset;
		}
		int writeReservedRegisters( void *from, void *to ) {
			int i = 0;
			int offset = 0;
			for ( auto reg : platforms::calling_convention::reserved_registers )
				offset += writeRegister( (std::uint8_t *)from + offset, (std::uint8_t *)to + sizeof( void * ) * i++, reg );
			return offset;
		}
		int readScratchRegisters( void *from, void *to ) {
			int i = 0;
			int offset = 0;
			for ( auto reg : platforms::calling_convention::scratch_registers )
				offset += readRegister( (std::uint8_t *)from + offset, (std::uint8_t *)to + sizeof( void * ) * i++, reg );
			return offset;
		}
		int readReservedRegisters( void *from, void *to ) {
			int i = 0;
			int offset = 0;
			for ( auto reg : platforms::calling_convention::reserved_registers )
				offset += readRegister( (std::uint8_t *)from + offset, (std::uint8_t *)to + sizeof( void * ) * i++, reg );
			return offset;
		}
		int setStackPointer( void *from, void *ptr ) { return MOV( (std::uintptr_t)from, (int)ptr, platforms::cpu::registers::sp ); }
	} // namespace detail
} // namespace llmo::assembler
