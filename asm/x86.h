#pragma once

namespace llmo::assembler {
	namespace branch::detail {
		int makeRelative( void *from, void *to, std::uint8_t opcode, int length ) {
			*(std::uint8_t *)from = opcode;
			*(std::uint32_t *)( (std::uintptr_t)from + 1 ) = (std::uintptr_t)to - ( (std::uintptr_t)from + length );
			return length;
		}
		int makeDirect( void *from, void *to, llmo::platforms::cpu::registers reg, std::uint8_t opcode ) {
			if ( reg > llmo::platforms::cpu::registers::EDI ) return 0;
			*(std::uint8_t *)from = 0xB8 + (int)reg;
			*(void **)( (std::uintptr_t)from + 1 ) = to;
			*( (std::uint8_t *)from + 5 ) = 0xFF;
			*( (std::uint8_t *)from + 6 ) = opcode + (int)reg;
			return 7;
		}
		int makeCondJmp( void *from, void *to, std::uint8_t cond ) {
			*(std::uint8_t *)from = 0x0F;
			*( (std::uint8_t *)from + 1 ) = cond;
			*(std::uint32_t *)( (std::uintptr_t)from + 2 ) = (std::uintptr_t)to - ( (std::uintptr_t)from + branch::consts::condJmpLen );
			return branch::consts::condJmpLen;
		}
	} // namespace branch::detail
	namespace detail {
		int writeRegister( void *from, void *to, platforms::cpu::registers reg ) {
			if ( reg == platforms::cpu::registers::EAX ) {
				*(std::uint8_t *)from = 0xA3;
				*(void **)( (std::uintptr_t)from + 1 ) = to;
				return 5;
			} else {
				*(std::uint8_t *)from = 0x89;
				*( (std::uint8_t *)from + 1 ) = 0x0D + ( (int)reg - (int)platforms::cpu::registers::ECX ) * 8;
				*(void **)( (std::uintptr_t)from + 2 ) = to;
				return 6;
			}
		}
		int readRegister( void *from, void *to, platforms::cpu::registers reg ) {
			if ( reg == platforms::cpu::registers::EAX ) {
				*(std::uint8_t *)from = 0xA1;
				*(void **)( (std::uintptr_t)from + 1 ) = to;
				return 5;
			} else {
				*(std::uint8_t *)from = 0x8B;
				*( (std::uint8_t *)from + 1 ) = 0x0D + ( (int)reg - (int)platforms::cpu::registers::ECX ) * 8;
				*(void **)( (std::uintptr_t)from + 2 ) = to;
				return 6;
			}
		}
		int writeScratchRegisters( void *from, void *to ) {
			int i = 0;
			int offset = 0;
			for ( auto reg : platforms::calling_convention::scratch_registers )
				offset += writeRegister( (std::uint8_t *)from + offset, (std::uint8_t *)to + sizeof( void * ) * i++, reg );
			return offset;
		}
		int writeReservedRegisters( void *from, void *to ) {
			int i = 0;
			int offset = 0;
			for ( auto reg : platforms::calling_convention::reserved_registers )
				offset += writeRegister( (std::uint8_t *)from + offset, (std::uint8_t *)to + sizeof( void * ) * i++, reg );
			return offset;
		}
		int readScratchRegisters( void *from, void *to ) {
			int i = 0;
			int offset = 0;
			for ( auto reg : platforms::calling_convention::scratch_registers )
				offset += readRegister( (std::uint8_t *)from + offset, (std::uint8_t *)to + sizeof( void * ) * i++, reg );
			return offset;
		}
		int readReservedRegisters( void *from, void *to ) {
			int i = 0;
			int offset = 0;
			for ( auto reg : platforms::calling_convention::reserved_registers )
				offset += readRegister( (std::uint8_t *)from + offset, (std::uint8_t *)to + sizeof( void * ) * i++, reg );
			return offset;
		}
		int setStackPointer( void *from, void *ptr ) {
			*( (std::uint8_t *)from ) = 0xBC;
			*(void **)( (std::uintptr_t)from + 1 ) = ptr;
			return 5;
		}
	} // namespace detail
} // namespace llmo::assembler
