#pragma once

namespace llmo::assembler {
	namespace branch::detail {
		int makeRelative( void *from, void *to, std::uint8_t opcode, int length ) {
			*(std::uint8_t *)from = opcode;
			*(std::uint32_t *)( (std::uintptr_t)from + 1 ) = (std::uintptr_t)to - ( (std::uintptr_t)from + length );
			return length;
		}
		int makeDirect( void *from, void *to, llmo::platforms::cpu::registers reg, std::uint8_t opcode ) {
			if ( reg > llmo::platforms::cpu::registers::R15 ) return 0;
			if ( reg < llmo::platforms::cpu::registers::R8 ) {
				*(std::uint8_t *)from = 0x48;
				*( (std::uint8_t *)from + 1 ) = 0xB8 + (int)reg;
			} else {
				*(std::uint8_t *)from = 0x49;
				*( (std::uint8_t *)from + 1 ) = 0xB8 + ( (int)reg - (int)llmo::platforms::cpu::registers::R8 );
			}
			*(void **)( (std::uintptr_t)from + 2 ) = to;
			*( (std::uint8_t *)from + 10 ) = 0xFF;
			*( (std::uint8_t *)from + 11 ) = opcode + (int)reg;
			return 12;
		}
		int makeCondJmp( void *from, void *to, std::uint8_t cond ) {
			*(std::uint8_t *)from = 0x0F;
			*( (std::uint8_t *)from + 1 ) = cond;
			*(std::uint32_t *)( (std::uintptr_t)from + 2 ) = (std::uintptr_t)to - ( (std::uintptr_t)from + branch::consts::condJmpLen );
			return branch::consts::condJmpLen;
		}
	} // namespace branch::detail
	namespace detail {
		int writeRegister( void *from, void *to, platforms::cpu::registers reg ) {
			if ( reg == platforms::cpu::registers::RAX ) {
				*(std::uint8_t *)from = 0x48;
				*( (std::uint8_t *)from + 1 ) = 0xA3;
				*(void **)( (std::uintptr_t)from + 2 ) = to;
				return 10;
			} else {
				*(std::uint8_t *)from = 0x50;
				*( (std::uint8_t *)from + 1 ) = 0x48;
				*( (std::uint8_t *)from + 2 ) = 0xB8;
				*(void **)( (std::uintptr_t)from + 3 ) = to;
				if ( reg < platforms::cpu::registers::R8 )
					*( (std::uint8_t *)from + 11 ) = 0x48;
				else
					*( (std::uint8_t *)from + 11 ) = 0x4c;
				*( (std::uint8_t *)from + 12 ) = 0x89;
				if ( reg < platforms::cpu::registers::R8 )
					*( (std::uint8_t *)from + 13 ) = (int)reg * 8;
				else
					*( (std::uint8_t *)from + 13 ) = ( (int)reg - (int)platforms::cpu::registers::R8 ) * 8;
				*( (std::uint8_t *)from + 14 ) = 0x58;
				return 15;
			}
		}
		int readRegister( void *from, void *to, platforms::cpu::registers reg ) {
			if ( reg == platforms::cpu::registers::RAX ) {
				*(std::uint8_t *)from = 0x48;
				*( (std::uint8_t *)from + 1 ) = 0xA1;
				*(void **)( (std::uintptr_t)from + 2 ) = to;
				return 10;
			} else {
				*(std::uint8_t *)from = 0x50;
				*( (std::uint8_t *)from + 1 ) = 0x48;
				*( (std::uint8_t *)from + 2 ) = 0xB8;
				*(void **)( (std::uintptr_t)from + 3 ) = to;
				if ( reg < platforms::cpu::registers::R8 )
					*( (std::uint8_t *)from + 11 ) = 0x48;
				else
					*( (std::uint8_t *)from + 11 ) = 0x4C;
				*( (std::uint8_t *)from + 12 ) = 0x8B;
				if ( reg < platforms::cpu::registers::R8 )
					*( (std::uint8_t *)from + 13 ) = (int)reg * 8;
				else
					*( (std::uint8_t *)from + 13 ) = ( (int)reg - (int)platforms::cpu::registers::R8 ) * 8;
				*( (std::uint8_t *)from + 14 ) = 0x58;
				return 15;
			}
		}
		int writeScratchRegisters( void *from, void *to ) {
			int i = 0;
			int offset = 0;
			for ( auto reg : platforms::calling_convention::scratch_registers )
				offset += writeRegister( (std::uint8_t *)from + offset, (std::uint8_t *)to + sizeof( void * ) * i++, reg );
			return offset;
		}
		int writeReservedRegisters( void *from, void *to ) {
			int i = 0;
			int offset = 0;
			for ( auto reg : platforms::calling_convention::reserved_registers )
				offset += writeRegister( (std::uint8_t *)from + offset, (std::uint8_t *)to + sizeof( void * ) * i++, reg );
			return offset;
		}
		int writeRegisters( void *from, void *to ) {
			int offset = writeRegister( from, to, platforms::cpu::registers::RAX );
			int i = 1;
			// mov rax, to + 8
			*( (std::uint8_t *)from + offset ) = 0x48;
			*( (std::uint8_t *)from + offset + 1 ) = 0xB8;
			*(void **)( (std::uintptr_t)from + offset + 2 ) = (std::uint8_t *)to + sizeof( void * ) * i++;
			offset += 10;
			auto writeReg = [&]( platforms::cpu::registers reg ) {
				// mov [rax], reg
				if ( reg < platforms::cpu::registers::R8 )
					*( (std::uint8_t *)from + offset ) = 0x48;
				else
					*( (std::uint8_t *)from + offset ) = 0x4c;
				*( (std::uint8_t *)from + offset + 1 ) = 0x89;
				if ( reg < platforms::cpu::registers::R8 )
					*( (std::uint8_t *)from + offset + 2 ) = (int)reg * 8;
				else
					*( (std::uint8_t *)from + offset + 2 ) = ( (int)reg - (int)platforms::cpu::registers::R8 ) * 8;
				offset += 3;
				// add rax, 8
				*( (std::uint8_t *)from + offset ) = 0x48;
				*( (std::uint8_t *)from + offset + 1 ) = 0x83;
				*( (std::uint8_t *)from + offset + 2 ) = 0xC0;
				*( (std::uint8_t *)from + offset + 3 ) = 0x08;
				offset += 4;
			};
			for ( auto reg : platforms::calling_convention::scratch_registers ) {
				if ( reg == platforms::cpu::registers::RAX ) continue;
				writeReg( reg );
			}
			for ( auto reg : platforms::calling_convention::reserved_registers ) {
				if ( reg == platforms::cpu::registers::RAX ) continue;
				writeReg( reg );
			}
			return offset;
		}
		int readScratchRegisters( void *from, void *to ) {
			int i = 0;
			int offset = 0;
			for ( auto reg : platforms::calling_convention::scratch_registers )
				offset += readRegister( (std::uint8_t *)from + offset, (std::uint8_t *)to + sizeof( void * ) * i++, reg );
			return offset;
		}
		int readReservedRegisters( void *from, void *to ) {
			int i = 0;
			int offset = 0;
			for ( auto reg : platforms::calling_convention::reserved_registers )
				offset += readRegister( (std::uint8_t *)from + offset, (std::uint8_t *)to + sizeof( void * ) * i++, reg );
			return offset;
		}
		int readRegisters( void *from, void *to ) {
			int offset = 0;
			int i = 1;
			// mov rax, to + 8
			*( (std::uint8_t *)from + offset ) = 0x48;
			*( (std::uint8_t *)from + offset + 1 ) = 0xB8;
			*(void **)( (std::uintptr_t)from + offset + 2 ) = (std::uint8_t *)to + sizeof( void * ) * i++;
			offset += 10;
			auto writeReg = [&]( platforms::cpu::registers reg ) {
				// mov [rax], reg
				if ( reg < platforms::cpu::registers::R8 )
					*( (std::uint8_t *)from + offset ) = 0x48;
				else
					*( (std::uint8_t *)from + offset ) = 0x4c;
				*( (std::uint8_t *)from + offset + 1 ) = 0x8B;
				if ( reg < platforms::cpu::registers::R8 )
					*( (std::uint8_t *)from + offset + 2 ) = (int)reg * 8;
				else
					*( (std::uint8_t *)from + offset + 2 ) = ( (int)reg - (int)platforms::cpu::registers::R8 ) * 8;
				offset += 3;
				// add rax, 8
				*( (std::uint8_t *)from + offset ) = 0x48;
				*( (std::uint8_t *)from + offset + 1 ) = 0x83;
				*( (std::uint8_t *)from + offset + 2 ) = 0xC0;
				*( (std::uint8_t *)from + offset + 3 ) = 0x08;
				offset += 4;
			};
			for ( auto reg : platforms::calling_convention::scratch_registers ) {
				if ( reg == platforms::cpu::registers::RAX ) continue;
				writeReg( reg );
			}
			for ( auto reg : platforms::calling_convention::reserved_registers ) {
				if ( reg == platforms::cpu::registers::RAX ) continue;
				writeReg( reg );
			}
			offset += readRegister( (std::uint8_t *)from + offset, to, platforms::cpu::registers::RAX );
			return offset;
		}
		int setStackPointer( void *from, void *ptr ) {
			*( (std::uint8_t *)from ) = 0x48;
			*( (std::uint8_t *)from + 1 ) = 0xBC;
			*(void **)( (std::uintptr_t)from + 2 ) = ptr;
			return 10;
		}
	} // namespace detail
} // namespace llmo::assembler
