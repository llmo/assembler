#include "asm.h"
#ifdef PLATFORM_ARCH_x86_64
#	include "asm/x86_64.h"
#elif defined( PLATFORM_ARCH_x86 )
#	include "asm/x86.h"
#elif defined( PLATFORM_ARCH_armv7a )
#	include "asm/armv7a.h"
#endif
#include <cstring>
#if defined( CAPSTONE )
#	if __has_include( "asm/capstone/include/capstone/capstone.h")
#		include "asm/capstone/include/capstone/capstone.h"
#	elif __has_include( <capstone/capstone.h>)
#		include <capstone/capstone.h>
#	else
#		static_assert(false, "Capstone enabled, but notfound");
#	endif
#endif

#if defined( PLATFORM_ARCH_x86 ) || defined( PLATFORM_ARCH_armv7a )
int llmo::assembler::branch::makeRelativeCall( void *from, void *to ) {
#	ifdef PLATFORM_ARCH_x86
#		ifdef PLATFORM_ARCH_x86_64
	if ( (std::size_t)to - (std::size_t)from > 0xFFFFFFFF ) return 0;
#		endif
	return detail::makeRelative( from, to, 0xE8, consts::relativeCallLen );
#	else
	if ( (std::size_t)to - (std::size_t)from > 0xFFFF ) return 0;
	auto branch = detail::makeBranch( (std::uint32_t)from, (std::uint32_t)to, true, true );
	std::memcpy( from, &branch, sizeof( std::uint32_t ) );
	return sizeof( std::uint32_t );
#	endif
}

int llmo::assembler::branch::makeRelativeJmp( void *from, void *to ) {
#	ifdef PLATFORM_ARCH_x86
#		ifdef PLATFORM_ARCH_x86_64
	if ( (std::size_t)to - (std::size_t)from > 0xFFFFFFFF ) return 0;
#		endif
	return detail::makeRelative( from, to, 0xE9, consts::relativeJmpLen );
#	else
	if ( (std::size_t)to - (std::size_t)from > 0xFFFF ) return 0;
	auto branch = detail::makeBranch( (std::uint32_t)from, (std::uint32_t)to );
	std::memcpy( from, &branch, sizeof( std::uint32_t ) );
	return sizeof( std::uint32_t );
#	endif
}

int llmo::assembler::branch::makeDirectCall( void *from, void *to, platforms::cpu::registers reg ) {
#	ifdef PLATFORM_ARCH_x86
	return detail::makeDirect( from, to, reg, 0xD0 );
#	else
	if ( reg >= platforms::cpu::registers::pc ) return 0;
	char instructions[10];
	assembler::detail::MOV( (uintptr_t)instructions, (int)to, reg );
	instructions[8] = 0x80 + (int)reg * 8;
	instructions[9] = 0x47;
	std::memcpy( from, &instructions, sizeof( instructions ) );
	return 10;
#	endif
}

#	ifdef PLATFORM_ARCH_x86
int llmo::assembler::branch::makeDirectJmp( void *from, void *to, platforms::cpu::registers reg ) {
	return detail::makeDirect( from, to, reg, 0xE0 );
}
#	endif

#	ifdef PLATFORM_ARCH_x86
int llmo::assembler::branch::makeCondJmpEq( void *from, void *to ) {
#		ifdef PLATFORM_ARCH_x86_64
	if ( (std::size_t)to - (std::size_t)from > 0xFFFFFFFF ) return 0;
#		endif
	return detail::makeCondJmp( from, to, 0x84 );
}

int llmo::assembler::branch::makeCondJmpNotEq( void *from, void *to ) {
#		ifdef PLATFORM_ARCH_x86_64
	if ( (std::size_t)to - (std::size_t)from > 0xFFFFFFFF ) return 0;
#		endif
	return detail::makeCondJmp( from, to, 0x85 );
}

int llmo::assembler::branch::makeCondJmpLess( void *from, void *to ) {
#		ifdef PLATFORM_ARCH_x86_64
	if ( (std::size_t)to - (std::size_t)from > 0xFFFFFFFF ) return 0;
#		endif
	return detail::makeCondJmp( from, to, 0x8C );
}

int llmo::assembler::branch::makeCondJmpGreat( void *from, void *to ) {
#		ifdef PLATFORM_ARCH_x86_64
	if ( (std::size_t)to - (std::size_t)from > 0xFFFFFFFF ) return 0;
#		endif
	return detail::makeCondJmp( from, to, 0x8F );
}

int llmo::assembler::branch::makeCondJmpGreatEq( void *from, void *to ) {
#		ifdef PLATFORM_ARCH_x86_64
	if ( (std::size_t)to - (std::size_t)from > 0xFFFFFFFF ) return 0;
#		endif
	return detail::makeCondJmp( from, to, 0x8D );
}

int llmo::assembler::branch::makeCondJmpLessEq( void *from, void *to ) {
#		ifdef PLATFORM_ARCH_x86_64
	if ( (std::size_t)to - (std::size_t)from > 0xFFFFFFFF ) return 0;
#		endif
	return detail::makeCondJmp( from, to, 0x8E );
}
#	endif

int llmo::assembler::makeNOP( void *addr, std::size_t size ) {
#	ifdef PLATFORM_ARCH_x86
	std::memset( addr, 0x90, size );
#	else
	for ( std::size_t i = 0; i < size; ++i ) std::memcpy( (std::uint8_t *)addr + i, "\x00\xBF", 2 );
#	endif
	return size;
}

int llmo::assembler::writeScratchRegistersToHeap( void *from, void *to ) {
	return detail::writeScratchRegisters( from, to );
}

int llmo::assembler::writeReservedRegistersToHeap( void *from, void *to ) {
	return detail::writeReservedRegisters( from, to );
}

int llmo::assembler::writeRegistersToHeap( void *from, void *to ) {
#	ifdef PLATFORM_ARCH_x86_64
	auto offset = detail::writeRegisters( from, to );
	offset += detail::readRegister( (std::uint8_t *)from + offset, to, platforms::cpu::registers::RAX );
	return offset;
#	else
	auto offset = writeScratchRegistersToHeap( from, to );
	auto scratchCount = sizeof( platforms::calling_convention::scratch_registers ) / sizeof( platforms::cpu::registers );
	return offset + writeReservedRegistersToHeap( (std::uint8_t *)from + offset, (std::uint8_t *)to + sizeof( void * ) * scratchCount );
#	endif
}

int llmo::assembler::readScratchRegistersFromHeap( void *from, void *to ) {
	return detail::readScratchRegisters( from, to );
}

int llmo::assembler::readReservedRegistersFromHeap( void *from, void *to ) {
	return detail::readReservedRegisters( from, to );
}

int llmo::assembler::readRegistersFromHeap( void *from, void *to ) {
#	ifdef PLATFORM_ARCH_x86_64
	return detail::readRegisters( from, to );
#	else
	auto offset = readScratchRegistersFromHeap( from, to );
	auto scratchCount = sizeof( platforms::calling_convention::scratch_registers ) / sizeof( platforms::cpu::registers );
	return offset + readReservedRegistersFromHeap( (std::uint8_t *)from + offset, (std::uint8_t *)to + sizeof( void * ) * scratchCount );
#	endif
}

int llmo::assembler::setStackPointer( void *from, void *to ) {
	return detail::setStackPointer( from, to );
}

#	ifdef PLATFORM_ARCH_x86
int llmo::assembler::writeFlagsToHeap( void *from, void *to ) {
	*(std::uint8_t *)from = 0x50;
	*( (std::uint8_t *)from + 1 ) = 0x9C;
	*( (std::uint8_t *)from + 2 ) = 0x58;
#		ifdef PLATFORM_ARCH_x86_64
	auto offset = detail::writeRegister( (std::uint8_t *)from + 3, to, platforms::cpu::registers::RAX );
#		else
	auto offset = detail::writeRegister( (std::uint8_t *)from + 3, to, platforms::cpu::registers::EAX );
#		endif
	*( (std::uint8_t *)from + offset + 3 ) = 0x58;
	return offset + 4;
}

int llmo::assembler::readFlagsFromHeap( void *from, void *to ) {
	*(std::uint8_t *)from = 0x50;
#		ifdef PLATFORM_ARCH_x86_64
	auto offset = detail::readRegister( (std::uint8_t *)from + 1, to, platforms::cpu::registers::RAX );
#		else
	auto offset = detail::readRegister( (std::uint8_t *)from + 1, to, platforms::cpu::registers::EAX );
#		endif
	*( (std::uint8_t *)from + offset + 1 ) = 0x50;
	*( (std::uint8_t *)from + offset + 2 ) = 0x9D;
	*( (std::uint8_t *)from + offset + 3 ) = 0x58;
	return offset + 4;
}

int llmo::assembler::popRetAddrToHeap( void *from, void *to ) {
	std::memset( from, 0x50, 2 ); // push rax; push rax
	int offset = 2;
#		ifdef PLATFORM_ARCH_x86_64
	std::memcpy( (std::uint8_t *)from + offset, "\x48\x8b\x44\x24\x10\x48\x89\x44\x24\x08", 10 );
	offset += 10;
#		else
	std::memcpy( (std::uint8_t *)from + offset, "\x8b\x44\x24\x08\x89\x44\x24\x04", 8 );
	offset += 8;
#		endif
	*( (std::uint8_t *)from + offset ) = 0x58;
	offset += 1;
#		ifdef PLATFORM_ARCH_x86_64
	std::memcpy( (std::uint8_t *)from + offset, "\x48\x89\x44\x24\x08", 5 );
	offset += 5;
#		else
	std::memcpy( (std::uint8_t *)from + offset, "\x89\x44\x24\x04", 4 );
	offset += 4;
#		endif
	*( (std::uint8_t *)from + offset ) = 0x58;
	offset += 1;
#		ifdef PLATFORM_ARCH_x86_64
	*( (std::uint8_t *)from + offset ) = 0x48;
	*( (std::uint8_t *)from + offset + 1 ) = 0xA3;
	offset += 2;
#		else
	*( (std::uint8_t *)from + offset ) = 0xA3;
	offset += 1;
#		endif
	*(void **)( (std::uint8_t *)from + offset ) = to;
	offset += sizeof( void * );
	*( (std::uint8_t *)from + offset ) = 0x58;
	return offset + 1;
}

int llmo::assembler::pushRetAddrFromHeap( void *from, void *to ) {
	std::memset( from, 0x50, 2 ); // push rax; push rax
	int offset = 2;
#		ifdef PLATFORM_ARCH_x86_64
	*( (std::uint8_t *)from + offset ) = 0x48;
	*( (std::uint8_t *)from + offset + 1 ) = 0xA1;
	offset += 2;
#		else
	*( (std::uint8_t *)from + offset ) = 0xA1;
	offset += 1;
#		endif
	*(void **)( (std::uint8_t *)from + offset ) = to;
	offset += sizeof( void * );
#		ifdef PLATFORM_ARCH_x86_64
	std::memcpy( (std::uint8_t *)from + offset, "\x48\x89\x44\x24\x08", 5 );
	offset += 5;
#		else
	std::memcpy( (std::uint8_t *)from + offset, "\x89\x44\x24\x04", 4 );
	offset += 4;
#		endif
	*( (std::uint8_t *)from + offset ) = 0x58;
	return offset + 1;
}

int llmo::assembler::writeCpuLiteToHeap( void *from, void *to ) {
#		ifdef PLATFORM_ARCH_x86_64
	auto offset = detail::writeRegisters( from, to );
#		else
	auto offset = writeRegistersToHeap( from, to );
#		endif
	*( (std::uint8_t *)from + offset ) = 0x9C;
	*( (std::uint8_t *)from + offset + 1 ) = 0x58;
	constexpr auto registers =
		( sizeof( platforms::calling_convention::scratch_registers ) + sizeof( platforms::calling_convention::reserved_registers ) ) /
		sizeof( platforms::cpu::registers );
#		ifdef PLATFORM_ARCH_x86_64
	offset += detail::writeRegister( (std::uint8_t *)from + offset + 2,
									 (std::uint8_t *)to + sizeof( void * ) * registers,
									 platforms::cpu::registers::RAX );
	offset += detail::readRegister( (std::uint8_t *)from + offset + 2, to, platforms::cpu::registers::RAX );
#		else
	offset += detail::writeRegister( (std::uint8_t *)from + offset + 2,
									 (std::uint8_t *)to + sizeof( void * ) * registers,
									 platforms::cpu::registers::EAX );
	offset += detail::readRegister( (std::uint8_t *)from + offset + 2, to, platforms::cpu::registers::EAX );
#		endif
	return offset + 2;
}

int llmo::assembler::readCpuLiteFromHeap( void *from, void *to ) {
	auto registers =
		( sizeof( platforms::calling_convention::scratch_registers ) + sizeof( platforms::calling_convention::reserved_registers ) ) /
		sizeof( platforms::cpu::registers );
#		ifdef PLATFORM_ARCH_x86_64
	auto offset = detail::readRegister( from, (std::uint8_t *)to + sizeof( void * ) * registers, platforms::cpu::registers::RAX );
#		else
	auto offset = detail::readRegister( from, (std::uint8_t *)to + sizeof( void * ) * registers, platforms::cpu::registers::EAX );
#		endif
	*( (std::uint8_t *)from + offset ) = 0x50;
	*( (std::uint8_t *)from + offset + 1 ) = 0x9D;
#		ifdef PLATFORM_ARCH_x86_64
	offset += detail::readRegisters( (std::uint8_t *)from + offset + 2, to );
#		else
	offset += readRegistersFromHeap( (std::uint8_t *)from + offset + 2, to );
#		endif
	return offset + 2;
}

int llmo::assembler::writeCpuToHeap( void *from, void *to ) {
	auto offset = writeCpuLiteToHeap( from, to );
#		ifdef PLATFORM_ARCH_x86_64
	offset -= 10;
#		else
	offset -= 5;
#		endif
	constexpr auto registers =
		( sizeof( platforms::calling_convention::scratch_registers ) + sizeof( platforms::calling_convention::reserved_registers ) ) /
		sizeof( platforms::cpu::registers );
	*( (std::uint8_t *)from + offset ) = 0x58;
	offset += 1;
#		ifdef PLATFORM_ARCH_x86_64
	*( (std::uint8_t *)from + offset ) = 0x48;
	*( (std::uint8_t *)from + offset + 1 ) = 0xA3;
	offset += 2;
#		else
	*( (std::uint8_t *)from + offset ) = 0xA3;
	offset += 1;
#		endif
	*(void **)( (std::uint8_t *)from + offset ) = (std::uint8_t *)to + sizeof( void * ) * ( registers + 1 );
	offset += sizeof( void * );
#		ifdef PLATFORM_ARCH_x86_64
	offset += detail::readRegister( (std::uint8_t *)from + offset, to, platforms::cpu::registers::RAX );
#		else
	offset += detail::readRegister( (std::uint8_t *)from + offset, to, platforms::cpu::registers::EAX );
#		endif
	return offset;
}

int llmo::assembler::readCpuFromHeap( void *from, void *to ) {
	auto registers =
		( sizeof( platforms::calling_convention::scratch_registers ) + sizeof( platforms::calling_convention::reserved_registers ) ) /
		sizeof( platforms::cpu::registers );
#		ifdef PLATFORM_ARCH_x86_64
	*(std::uint8_t *)from = 0x48;
	*( (std::uint8_t *)from + 1 ) = 0xA1;
	int offset = 2;
#		else
	*(std::uint8_t *)from = 0xA1;
	int offset = 1;
#		endif
	*(void **)( (std::uint8_t *)from + offset ) = (std::uint8_t *)to + sizeof( void * ) * ( registers + 1 );
	offset += sizeof( void * );
	*( (std::uint8_t *)from + offset ) = 0x50;
	offset += readCpuLiteFromHeap( (std::uint8_t *)from + offset + 1, to );
	return offset;
}
#	endif

#	if defined( CAPSTONE )
int llmo::assembler::getCodeLength( void *addr, int minLength ) {
	static auto arch = PLATFORM_ARCH == llmo::platforms::arch::armv7a ? CS_ARCH_ARM : CS_ARCH_X86;
	static auto mode = PLATFORM_ARCH == llmo::platforms::arch::x86_64 ?
						   CS_MODE_64 :
							 ( PLATFORM_ARCH == llmo::platforms::arch::x86 ? CS_MODE_32 : CS_MODE_16 );
	csh handle;
	if ( cs_open( arch, mode, &handle ) != CS_ERR_OK ) return 0;

	cs_insn *insn;
	auto count = cs_disasm( handle, (uint8_t *)addr, 24, (uint64_t)addr, 0, &insn );
	if ( count <= 0 ) {
		cs_close( &handle );
		return 0;
	}

	int result = 0;
	for ( decltype( count ) j = 0; j < count; j++ ) {
		result += insn[j].size;
		if ( result >= minLength ) break;
	}

	cs_free( insn, count );
	cs_close( &handle );

	return result;
}
#	elif defined( PLATFORM_ARCH_x86 )
int getCodeLength( void *addr, int minLength ) {
	auto pCode = reinterpret_cast<std::uint8_t *>( addr );
	switch ( *pCode ) {
		case 0xE8:
			[[fallthrough]];
		case 0xE9:
			[[fallthrough]];
		case 0xA3:
			[[fallthrough]];
		case 0xA1:
			[[fallthrough]];
		case 0xA0:
			[[fallthrough]];
		case 0xA2:
			[[fallthrough]];
		case 0xA9:
			[[fallthrough]];
		case 0xB8:
			[[fallthrough]];
		case 0xB9:
			[[fallthrough]];
		case 0xBA:
			[[fallthrough]];
		case 0xBB:
			[[fallthrough]];
		case 0xBD:
			[[fallthrough]];
		case 0xBE:
			[[fallthrough]];
		case 0xBF:
			[[fallthrough]];
		case 0xBC:
			[[fallthrough]];
		case 0x35:
			[[fallthrough]];
		case 0x3D:
			[[fallthrough]];
		case 0x0D:
			[[fallthrough]];
		case 0x15:
			[[fallthrough]];
		case 0x1D:
			[[fallthrough]];
		case 0x25:
			[[fallthrough]];
		case 0x2D:
			[[fallthrough]];
		case 0x68:
			[[fallthrough]];
		case 0x05:
			return 5;
		case 0x89:
			[[fallthrough]];
		case 0x8B:
			[[fallthrough]];
		case 0x69:
			[[fallthrough]];
		case 0x81:
			[[fallthrough]];
		case 0xC7:
			[[fallthrough]];
		case 0xF7:
			[[fallthrough]];
		case 0x0F:
			return 6;
		case 0xEA:
			[[fallthrough]];
		case 0x9A:
			return 7;
		default:
			return 0;
	}
}
#	endif

#endif
