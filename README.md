[![pipeline status](https://gitlab.com/llmo/assembler/badges/main/pipeline.svg)](https://gitlab.com/llmo/assembler/-/commits/main)

# Assembler

Unified assembler operations for i686, x86_64, armv7a

## Usage
1. Add project as subproject
```cmake
add_subdirectory(assembler)
```
2. Link library
```cmake
target_link_libraries($(PROJECT_NAME) PRIVATE llmo_asm)
```
3. Include headers to global space
```cmake
include_directories(assembler)
```
4. ???
5. PROFIT
```c++
#include <iostream>
#include <asm.h>

namespace assembler = llmo::assembler;

void foo(){
    std::cout << "call foo" << std::endl;
}

void bar(){
    std::cout << "call bar" << std::endl;
}

int main(){
    foo(); // print "call foo"
    
    // You need PROT_WRITE at foo! e.g. you can use llmo::mem
    // llmo::mem::prot::set( (void*)&foo );
	assembler::branch::makeRelativeJmp( (void*)&foo, (void*)&bar );
    
    foo(); // print "call bar"
    bar(); // print "call bar"
	
	return 0;
}
```
