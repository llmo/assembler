#pragma once

#include "platforms.hpp"

namespace llmo::assembler {
	namespace branch {
		namespace consts {
#ifdef PLATFORM_ARCH_x86_64
			static const auto relativeCallLen = 5;
			static const auto relativeJmpLen = 5;
			static const auto condJmpLen = 6;
			static const auto directCallLen = 12;
			static const auto directJmpLen = 12;
#elif defined( PLATFORM_ARCH_x86 )
			static const auto relativeCallLen = 5;
			static const auto relativeJmpLen = 5;
			static const auto condJmpLen = 6;
			static const auto directCallLen = 7;
			static const auto directJmpLen = 7;
#elif defined( PLATFORM_ARCH_armv7a )
			static const auto relativeCallLen = 4;
			static const auto relativeJmpLen = 4;
			static const auto condJmpLen = 4;
			static const auto directCallLen = 10;
			static const auto directJmpLen = 10;
#endif
		} // namespace consts
#if defined( PLATFORM_ARCH_x86 ) || defined( PLATFORM_ARCH_armv7a )
		int makeRelativeCall( void *from, void *to );
		int makeRelativeJmp( void *from, void *to );
		int makeDirectCall( void *from, void *to, platforms::cpu::registers reg );
		int makeDirectJmp( void *from, void *to, platforms::cpu::registers reg );
#	ifndef PLATFORM_ARCH_armv7a
		int makeCondJmpEq( void *from, void *to );
		int makeCondJmpNotEq( void *from, void *to );
		int makeCondJmpLess( void *from, void *to );
		int makeCondJmpGreat( void *from, void *to );
		int makeCondJmpGreatEq( void *from, void *to );
		int makeCondJmpLessEq( void *from, void *to );
#	endif
#endif
	} // namespace branch
	namespace consts {
#ifdef PLATFORM_ARCH_x86_64
		static const auto NOPLen = 1;
		static const auto writeToRegisterLen = 10;
		static const auto pushLen = 24;
		static const auto pushRegisterLen = 1;
		static const auto writeRegisterToHeapLen = 15;
#elif defined( PLATFORM_ARCH_x86 )
		static const auto NOPLen = 1;
		static const auto writeToRegisterLen = 5;
		static const auto pushLen = 5;
		static const auto pushRegisterLen = 2;
		static const auto writeRegisterToHeapLen = 6;
#elif defined( PLATFORM_ARCH_armv7a )
		static const auto NOPLen = 2;
		static const auto writeToRegisterLen = 8;
		static const auto pushLen = 16;
		static const auto pushRegisterLen = 2;
		static const auto writeRegisterToHeapLen = 22;
#endif
	} // namespace consts
#if defined( PLATFORM_ARCH_x86 ) || defined( PLATFORM_ARCH_armv7a )
	int makeNOP( void *addr, std::size_t size );
	int writeScratchRegistersToHeap( void *from, void *to );
	int writeReservedRegistersToHeap( void *from, void *to );
	int writeRegistersToHeap( void *from, void *to );
	int readScratchRegistersFromHeap( void *from, void *to );
	int readReservedRegistersFromHeap( void *from, void *to );
	int readRegistersFromHeap( void *from, void *to );
	int setStackPointer( void *from, void *to );
#	if defined( PLATFORM_ARCH_x86 )
	int writeFlagsToHeap( void *from, void *to );
	int readFlagsFromHeap( void *from, void *to );
	int popRetAddrToHeap( void *from, void *to );
	int pushRetAddrFromHeap( void *from, void *to );
	int writeCpuLiteToHeap( void *from, void *to );
	int readCpuLiteFromHeap( void *from, void *to );
	int writeCpuToHeap( void *from, void *to );
	int readCpuFromHeap( void *from, void *to );
#	endif
#	if defined( CAPSTONE )
#		if defined( PLATFORM_ARCH_armv7a )
	int getCodeLength( void *addr, int minLength = 4 );
#		else
	int getCodeLength( void *addr, int minLength = 5 );
#		endif
#	elif defined( PLATFORM_ARCH_x86 )
	int getCodeLength( void *addr, int minLength = 5 );
#	endif
#endif
} // namespace llmo::assembler

#if __has_include_next( <asm.h>)
#	include_next <asm.h>
#endif
