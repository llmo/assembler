#pragma once

#include <cstdint>
#include <cstddef>

namespace llmo::platforms {
	enum class arch
	{
		unsupported,
		x86,
		x86_64,
		armv7a,
		aarch64
	};

	enum class abi
	{
		unsupported,
		SysV,
		SysV64,
		MS,
		MS64,
		ARM
	};
} // namespace llmo::platforms

#ifndef PLATFORM_ARCH
#	if defined( __i686 ) || defined( __i686__ ) || defined( __i386 ) || defined( _X86_ ) || defined( i386 ) || defined( __i386__ )
#		define PLATFORM_ARCH llmo::platforms::arch::x86
#		define PLATFORM_ARCH_x86
#	elif defined( __amd64 ) || defined( __x86_64 ) || defined( __x86_64__ ) || defined( __amd64__ )
#		define PLATFORM_ARCH llmo::platforms::arch::x86_64
#		define PLATFORM_ARCH_x86
#		define PLATFORM_ARCH_x86_64
#	elif defined( __arm ) || defined( __arm__ ) || ( defined( __ARM_ARCH ) && __ARM_ARCH == 4 )
#		define PLATFORM_ARCH llmo::platforms::arch::armv7a
#		define PLATFORM_ARCH_armv7a
#	elif defined( __aarch64__ ) || ( defined( __ARM_ARCH ) && __ARM_ARCH >= 8 )
#		define PLATFORM_ARCH llmo::platforms::arch::aarch64
#		define PLATFORM_ARCH_aarch64
#	else
#		define PLATFORM_ARCH llmo::platforms::arch::unsupported
#	endif
#endif

#ifndef PLATFORM_ABI
#	if defined( WIN64 ) || defined( _WIN64 ) || defined( __WIN64 ) || defined( __WIN64__ )
#		define PLATFORM_ABI llmo::platforms::abi::MS64
#		define PLATFORM_ABI_WINDOWS
#	elif defined( WIN32 ) || defined( _WIN32 ) || defined( __WIN32 ) || defined( __WIN32__ )
#		define PLATFORM_ABI llmo::platforms::abi::MS
#		define PLATFORM_ABI_WINDOWS
#	elif defined( __unix ) || defined( __unix__ ) || defined( unix )
#		define PLATFORM_ABI_POSIX
#		if defined( __arm ) || defined( __arm__ ) || defined( __aarch64__ ) || defined( __ARM_ARCH )
#			define PLATFORM_ABI llmo::platforms::abi::ARM
#			define PLATFORM_ABI_arm
#		elif defined( __x86_64 ) || defined( __x86_64__ )
#			define PLATFORM_ABI llmo::platforms::abi::SysV64
#			define PLATFORM_ABI_POSIX_SysV64
#		elif defined( __i386 ) || defined( i386 ) || defined( __i386__ )
#			define PLATFORM_ABI llmo::platforms::abi::SysV
#			define PLATFORM_ABI_POSIX_SysV
#		else
#			define PLATFORM_ABI llmo::platforms::abi::unsupported
#		endif
#	else
#		define PLATFORM_ABI llmo::platforms::abi::unsupported
#	endif
#endif

namespace llmo::platforms {
	struct cpu {
		enum class registers
		{
#ifdef PLATFORM_ARCH_x86_64
#	define REG( reg ) R##reg
#elif defined( PLATFORM_ARCH_x86 )
#	define REG( reg ) E##reg
#endif
#ifdef PLATFORM_ARCH_x86
			REG( AX ),
			REG( CX ),
			REG( DX ),
			REG( BX ),
			REG( SP ),
			REG( BP ),
			REG( SI ),
			REG( DI ),
#	ifdef PLATFORM_ARCH_x86_64
			REG( 8 ),
			REG( 9 ),
			REG( 10 ),
			REG( 11 ),
			REG( 12 ),
			REG( 13 ),
			REG( 14 ),
			REG( 15 ),
#	endif
//			REG( IP ),
//			CS,
//			DS,
//			SS,
//			ES,
//			FS,
//			GS,
#else
			r0,
			r1,
			r2,
			r3,
			r4,
			r5,
			r6,
			r7,
			r8,
			r9,
			r10,
			r11,
			r12,
			r13,
			r14,
			r15,
			sb = r9,
			sl,
			fp,
			ip,
			sp,
			lr,
			pc,
#endif
			count
#ifdef REG
#	undef REG
#endif
		};

#ifdef PLATFORM_ARCH_x86
#	ifndef CPURX
#		define CPURX( reg ) \
			union { \
				union { \
					struct { \
						std::uint8_t reg##L; \
						std::uint8_t reg##H; \
					}; \
					std::uint16_t reg##X; \
				}; \
				std::uint32_t E##reg##X; \
			};
#	endif
#	ifndef CPUR
#		define CPUR( reg ) \
			union { \
				std::uint8_t reg##L; \
				std::uint16_t reg; \
				std::uint32_t E##reg; \
			};
#	endif
#	ifdef PLATFORM_ARCH_x86_64
#		define REGX( reg ) \
			union { \
				CPURX( reg ) \
				std::uint64_t R##reg##X; \
			};
#		define REG( reg ) \
			union { \
				CPUR( reg ) \
				std::uint64_t R##reg; \
			};
#		define REGD( reg ) \
			union { \
				std::uint8_t R##reg##B; \
				std::uint16_t R##reg##W; \
				std::uint32_t R##reg##D; \
				std::uint64_t R##reg; \
			};
#	else
#		define REGX( reg ) CPURX( reg )
#		define REG( reg ) CPUR( reg )
#	endif
#	if defined( PLATFORM_ABI_WINDOWS ) || !defined( PLATFORM_ARCH_x86_64 )
		REGX( A )
		REGX( C )
		REGX( D )
		REGX( B )
		REG( SP )
		REG( BP )
		REG( SI )
		REG( DI )
#		ifdef PLATFORM_ARCH_x86_64
		REGD( 8 )
		REGD( 9 )
		REGD( 10 )
		REGD( 11 )
		REGD( 12 )
		REGD( 13 )
		REGD( 14 )
		REGD( 15 )
#		endif
#	else
		REGX( A )
		REG( DI )
		REG( SI )
		REGX( D )
		REGX( C )
#		ifdef PLATFORM_ARCH_x86_64
		REGD( 8 )
		REGD( 9 )
		REGD( 10 )
		REGD( 11 )
#		endif
		REGX( B )
		REG( SP )
		REG( BP )
#		ifdef PLATFORM_ARCH_x86_64
		REGD( 12 )
		REGD( 13 )
		REGD( 14 )
		REGD( 15 )
#		endif
#	endif
//		std::uint16_t CS, DS, SS, ES, FS, GS;
#	ifdef PLATFORM_ARCH_x86_64
		union {
#	endif
			union {
				struct {
					union {
						struct {
							std::uint8_t CF		   : 1;
							std::uint8_t RESERVE1  : 1;
							std::uint8_t PF		   : 1;
							std::uint8_t RESERVE3  : 1;
							std::uint8_t AF		   : 1;
							std::uint8_t RESERVE5  : 1;
							std::uint8_t ZF		   : 1;
							std::uint8_t SF		   : 1;

							std::uint8_t TF		   : 1;
							std::uint8_t IF		   : 1;
							std::uint8_t DF		   : 1;
							std::uint8_t OF		   : 1;
							std::uint8_t IOPL	   : 2;
							std::uint8_t NT		   : 1;
							std::uint8_t RESERVE15 : 1;
						};
						std::uint16_t FLAGS;
					};

					std::uint8_t RF		   : 1;
					std::uint8_t VM		   : 1;
					std::uint8_t AC		   : 1;
					std::uint8_t VIF	   : 1;
					std::uint8_t VIP	   : 1;
					std::uint8_t ID		   : 1;
					std::uint8_t RESERVE22 : 2;

					std::uint8_t RESERVE24;
				};
				std::uint32_t EFLAGS;
			};
#	ifdef PLATFORM_ARCH_x86_64
			std::uint64_t RFLAGS;
		};
#		undef REGD
#	endif
#	undef REG
#	undef REGX
#	ifdef CPURX
#		undef CPURX
#	endif
#	ifdef CPUR
#		undef CPUR
#	endif
#else
		std::size_t r0, r1, r2, r3, ip, r4, r5, r6, r7, r8, sb, sl, fp, sp, lr;
#endif
	};

#ifdef PLATFORM_ARCH_x86
	struct fpu {
		using xmm = std::uint8_t[16];
#	pragma pack( push, 1 )
		struct stmm {
			std::uint8_t reserved[6];
			union {
				float fValue;
				double dValue;
				std::uint8_t value[10];
			};
		};
#	pragma pack( pop )
		static_assert( sizeof( stmm ) == 16 );

#	ifdef PLATFORM_ARCH_x86_64
		std::uint64_t FIP;
#	else
		std::uint16_t reserved;
		std::uint16_t FCS;
		std::uint32_t FIP;
#	endif
		std::uint16_t FOP;
		std::uint8_t reserved2;
		std::uint8_t FTW;
		std::uint16_t FSW;
		std::uint16_t FCW;

		std::uint32_t MXCSR_MASK;
		std::uint32_t MXCSR;
#	ifdef PLATFORM_ARCH_x86_64
		std::uint64_t FDP;
#	else
		std::uint16_t reserved3;
		std::uint16_t FDS;
		std::uint32_t FDP;
#	endif

		stmm ST[8];
		xmm XMM[16];

		std::uint8_t reserved4[3][16];
		std::uint8_t available[3][16];
	};
	static_assert( sizeof( fpu ) == 512 );
#endif

	namespace calling_convention {
#ifdef PLATFORM_ABI_WINDOWS
#	ifdef PLATFORM_ARCH_x86_64
		static const cpu::registers return_value[] = { cpu::registers::RAX };
		static const cpu::registers parametr_registers[] = { cpu::registers::RCX,
															 cpu::registers::RDX,
															 cpu::registers::R8,
															 cpu::registers::R9 };
		static const cpu::registers scratch_registers[] = { cpu::registers::RAX, cpu::registers::RCX, cpu::registers::RDX,
															cpu::registers::R8,	 cpu::registers::R9,  cpu::registers::R10,
															cpu::registers::R11 };
		static const cpu::registers reserved_registers[] = { cpu::registers::RBX, cpu::registers::RSP, cpu::registers::RBP,
															 cpu::registers::RSI, cpu::registers::RDI, cpu::registers::R12,
															 cpu::registers::R13, cpu::registers::R14, cpu::registers::R15 };
		static const cpu::registers call_list[] = { cpu::registers::RBP };
		static const cpu::registers thiscall_registers[] = {};
#	else
		static const cpu::registers return_value[] = { cpu::registers::EAX };
		static const cpu::registers parametr_registers[] = {};
		static const cpu::registers scratch_registers[] = { cpu::registers::EAX, cpu::registers::ECX, cpu::registers::EDX };
		static const cpu::registers reserved_registers[] = { cpu::registers::EBX,
															 cpu::registers::ESP,
															 cpu::registers::EBP,
															 cpu::registers::ESI,
															 cpu::registers::EDI };
		static const cpu::registers call_list[] = { cpu::registers::EBP };
		static const cpu::registers thiscall_registers[] = { cpu::registers::ECX };
#	endif
#else
#	ifdef PLATFORM_ARCH_x86_64
		static const cpu::registers return_value[] = { cpu::registers::RAX, cpu::registers::RDX };
		static const cpu::registers parametr_registers[] = { cpu::registers::RDI, cpu::registers::RSI, cpu::registers::RDX,
															 cpu::registers::RCX, cpu::registers::R8,  cpu::registers::R9 };
		static const cpu::registers scratch_registers[] = { cpu::registers::RAX, cpu::registers::RDI, cpu::registers::RSI,
															cpu::registers::RDX, cpu::registers::RCX, cpu::registers::R8,
															cpu::registers::R9,	 cpu::registers::R10, cpu::registers::R11 };
		static const cpu::registers reserved_registers[] = { cpu::registers::RBX, cpu::registers::RSP, cpu::registers::RBP,
															 cpu::registers::R12, cpu::registers::R13, cpu::registers::R14,
															 cpu::registers::R15 };
		static const cpu::registers call_list[] = { cpu::registers::RBP };
		static const cpu::registers thiscall_registers[] = {};
#	elif defined( PLATFORM_ARCH_x86 )
		static const cpu::registers return_value[] = { cpu::registers::EAX, cpu::registers::EDX };
		static const cpu::registers parametr_registers[] = {};
		static const cpu::registers scratch_registers[] = { cpu::registers::EAX, cpu::registers::ECX, cpu::registers::EDX };
		static const cpu::registers reserved_registers[] = { cpu::registers::EBX,
															 cpu::registers::ESP,
															 cpu::registers::EBP,
															 cpu::registers::ESI,
															 cpu::registers::EDI };
		static const cpu::registers call_list[] = { cpu::registers::EBP };
		static const cpu::registers thiscall_registers[] = {};
#	else
		static const cpu::registers return_value[] = { cpu::registers::r0, cpu::registers::r1 };
		static const cpu::registers parametr_registers[] = { cpu::registers::r0,
															 cpu::registers::r1,
															 cpu::registers::r2,
															 cpu::registers::r3 };
		static const cpu::registers scratch_registers[] = { cpu::registers::r0,
															cpu::registers::r1,
															cpu::registers::r2,
															cpu::registers::r3,
															cpu::registers::r12 };
		static const cpu::registers reserved_registers[] = { cpu::registers::r4,  cpu::registers::r5,  cpu::registers::r6,
															 cpu::registers::r7,  cpu::registers::r8,  cpu::registers::r9,
															 cpu::registers::r10, cpu::registers::r11, cpu::registers::r13,
															 cpu::registers::r14 };
		static const cpu::registers call_list[] = {};
		static const cpu::registers thiscall_registers[] = {};
#	endif
#endif
	} // namespace calling_convention

} // namespace llmo::platforms
